function displayError(message){
	$("#error").fadeIn("slow", function() {
	    $(this).removeClass("hidden");
	});   	
	$("#error").html('<i class="fa fa-exclamation-triangle"></i> ' + message  + ' <a href="#"  class="close" aria-label="close">&times;</a>');
}

function logout() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", "logout.php", true);
        xmlhttp.send();
    }

