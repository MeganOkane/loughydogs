<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class HomeController extends Controller
{
    public function create(){

        return view('pages.index')->with('uri_tail', "home");

    }

    public function postContact(Request $request){

        $this->validate($request, [
            'email' => 'required|email',
            'subject' => 'min:3',
            'message' => 'min:10'
        ]);

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'bodyMessage' => $request->message
        );

        Mail::send('emails.create',$data,function($message) use ($data){
            $message->from("info@loughydogs.com");
            $message->replyTo($data['email']);
            $message->to('info@loughydogs.com');
            $message->subject($data['subject']);
        });

        Session::flash('success', 'Thanks for the message! We will get back to you soon.');

        $url = URL::route('home') . '#contactus';
        return Redirect::to($url);

    }

}
