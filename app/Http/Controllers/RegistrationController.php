<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class RegistrationController extends Controller
{
    public function create(){

        return view('registration.create');

    }

    public function store(){

        //validate the form

        $this->validate(request(), [
            'first_name' => 'required',
            'last_name' => 'required',
//            'address' => 'required',
//            'city' => 'required',
//            'country' => 'required',
            'postcode' => 'required',
//            'dob' => 'required',
            'contact_number' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed'
        ]);

        //create and save the user

        $user = User::create([
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'email' => request('email'),
            'postcode' => request('postcode'),
            'address' => "14 Croaghan Gardens",
            'city' => "Belfast",
            'country' => "Ireland",
            'contact_number' => request('contact_number'),
            'password' => bcrypt(request('password'))
        ]);

        //sign them in

        auth()->login($user);

        //redirect to the home page

        return redirect()->home();

    }

}
