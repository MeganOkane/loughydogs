<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;


class ContactUsController extends Controller
{

    public function contactUs(){
        return view('pages.contactus')->with('uri_tail', "contact");
    }

    public function postContact(Request $request){

        $this->validate($request, [
            'email' => 'required|email',
            'subject' => 'min:3',
            'message' => 'min:10'
        ]);

        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'bodyMessage' => $request->message
        );

        Mail::send('emails.create',$data,function($message) use ($data){
            $message->from('info@loughydogs.com');
            $message->replyTo($data['email']);
            $message->to('info@loughydogs.com');
            $message->subject($data['subject']);
        });

        Session::flash('success', 'Thanks for the message! We will get back to you soon.');

        return redirect()->route('getContact');

    }
}
