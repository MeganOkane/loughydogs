

<div class="footerbg" >
    <div class="row">
        <div class="footer-contents">
            <h4 class="_14">Follow us on</h4>
                <ul class="footer-contents-links text-center">
                    <li><a href="https://www.facebook.com/loughydogs/" class="footer-content"> <i class="fa fa-facebook footer-icon"></i></a></li>
                    <li><a href="https://www.twitter.com/loughydogs" class="footer-content"> <i class="fa fa-twitter footer-icon"></i></a></li>
                    <li><a href="https://www.instagram.com/loughydogs/?hl=en" class="footer-content"> <i class="fa fa-instagram footer-icon"></i></a></li>
                </ul>
        </div>
    </div>

    <div class="row footer-row-centered">
        <div class="footer-contents">
            <ul class="footer-contents-links text-center">
                <li class="footer-content white"> Loughy Dogs Copyright 2018 © | </li>
                <li><a href="{{url('privacy')}}" class="footer-content"> Privacy Policy | </a></li>
                <li><a href="{{url('terms')}}" class="footer-content"> Terms and Conditions </a></li>
            </ul>
        </div>
    </div>
</div>

</body>

</html>

