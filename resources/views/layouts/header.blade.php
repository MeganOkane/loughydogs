 <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
    <meta name="keywords" content="Loughy Dogs, Magherafelt, Mid-Ulster, dog kennels, dog day care, lough neagh, Derry dog kennels, Belfast, north east, maghera, cookstown, toomebridge
     Northern Ireland, boarding kennels, dog sitting, castledawson, international airport, antrim, large space, cheap, inside kennels, heated, exercise yard, lots of walks, pickups, dog kennel pcitures,
     visit dog kennels, open on sundays, contact number, email, loughy dogs website">
    <title>@yield('pageTitle') - Loughy Dogs</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!--Bootstrap & Custom CSS-->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/common.css') }}">
    @if(isset($uri_tail))<link rel="stylesheet" href="{{asset('css/'.$uri_tail.".css" )}}"/>@endif
    <!--Bootstrap & JQuery-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <!--Font Awesome-->
    <script src="https://use.fontawesome.com/db7cbfd4be.js"></script>
    <!--Animations-->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">

    <link rel="icon" type="image/png" href="{{url('img/favicon/favicon-32x32.png')}}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{url('img/favicon/favicon-16x16.png')}}" sizes="16x16" />

</head>

<body>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <p class="menu-title-mobile">Menu</p>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="logoGroup">
                <a class="navbar-brand" style="padding:0;" href="{{url('/index')}}"><img src="img/loughylogo.png" id="logoImg" alt="Loughy Dogs Logo" /></a>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <div class="ul-center">
                <ul class="nav navbar-nav navbar-right nav-center">
                    <li><a href="{{url('/')}}">HOME</a></li>
                    <li><a href="aboutus">ABOUT</a></li>
                    <li><a href="services">SERVICES</a></li>
                    <li><a href="gallery">GALLERY</a></li>
                    <li><a href="faqs">FAQS</a></li>
                    <li><a href="contactus">CONTACT</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>
