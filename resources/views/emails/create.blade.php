<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body style="margin:0; padding:0; font-family:'Arial', 'Helvetica', sans-serif;">

<div style="margin: 0 auto; max-width:100%;">

    <img src="http://loughydogs.com/img/loughy_outline.png" style="width:30%; display:block; margin-left: auto; margin-right: auto;"/>

    <div style="background-color:#22A7F0; padding:50px; margin:0; color:white; height:100%;">

        <h1 style="text-align:center;">You have received a message from {{$name}}</h1>
        <span style="display:block; font-size:1.2em;text-align:center; margin-bottom:20px;"><strong>E-mail: </strong>{{$email}}</span>

        <div style="background-color: #777777; border-radius:25px; margin: 0 auto; max-width:100%;">

            <div style="padding:30px;">
                <span style="font-size:1.5em;"><strong>Subject:</strong> {{$subject}}</span>

                <hr/>

                <p><strong>Message:</strong></p>
                <p>{{$bodyMessage}}</p>
            </div>
        </div>

    </div>

</div>

<div>

    <a href="http://www.loughydogs.com" style="display:block; text-align:center; padding:30px;">www.loughydogs.com</a>

</div>

</body>
</html>