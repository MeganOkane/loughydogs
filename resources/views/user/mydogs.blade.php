@extends('layouts.master')

@section('content')

    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:300px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b> <i class="fa fa-users fa-fw"></i> Manage Users</b></h5>
        </header>

        <table class="table table-striped table-hover text-center">
            <thead>
            <tr>
                <th class="text-center">User ID</th>
                <th class="text-center">Firstname</th>
                <th class="text-center">Lastname</th>
                <th class="text-center">Email</th>
                <th class="text-center">Address</th>
                <th class="text-center">City</th>
                <th class="text-center">Country</th>
                <th class="text-center">Postcode</th>
                <th class="text-center">Home Number</th>
                <th class="text-center">Mobile Number</th>
                <th class="text-center">Date of Birth</th>
                <th class="text-center">Role</th>
            </tr>
            </thead>
            <tbody>

            <?php loadUsers() ?>

            </tbody>
        </table>
    </div>


@endsection