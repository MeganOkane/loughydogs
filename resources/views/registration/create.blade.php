@extends('layouts.master')

@section('content')

    <div class="container">
        <div class="row main">
            <div class="alert alert-danger alert-dismissable hidden">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> Indicates a successful or positive action.
            </div>
            <div class="panel-heading">
                <div class="panel-title text-center">
                    <h1 class="title">Create <strong>your</strong> Loughy Dogs account</h1>
                    <hr />
                </div>
            </div>
            <div class="main-login main-center register">
                <form class="form-horizontal" method="post" action="{{url('/register')}}">

                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="first_name" class="cols-sm-2 control-label">First Name:</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa bluefont" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="first_name" id="first_name"  placeholder="Enter your first name"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="last_name" class="cols-sm-2 control-label">Last Name:</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa bluefont" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="last_name" id="last_name"  placeholder="Enter your last name"/>
                            </div>
                        </div>
                    </div>

            {{--        <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Address:</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa bluefont" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="address" id="address"  placeholder="Enter your address"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">City:</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa bluefont" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="city" id="city"  placeholder="Enter your city"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Country:</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa bluefont" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="country" id="country"  placeholder="Enter your country"/>
                            </div>
                        </div>
                    </div>--}}

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Postcode:</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa bluefont" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="postcode" id="postcode"  placeholder="Enter your postcode"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Contact Number:</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa bluefont" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="contact_number" id="contact_number"  placeholder="Enter your contact number"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="cols-sm-2 control-label">Email:</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope fa bluefont" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="email" id="email"  placeholder="Enter your e-mail"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="cols-sm-2 control-label">Password:</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock fa-lg bluefont" aria-hidden="true"></i></span>
                                <input type="password" class="form-control" name="password" id="password"  placeholder="Enter your password"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="confirm" class="cols-sm-2 control-label">Confirm Password:</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock fa-lg bluefont" aria-hidden="true"></i></span>
                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation"  placeholder="Confirm your password"/>
                            </div>
                        </div>
                    </div>

                    <div class="register">
                        <button id="buttonlogintoregister" name="submit" type="submit">Register</button>
                    </div>

                    <div class="login-register">
                        <a href="{{url('/login')}}">Login</a>
                    </div>

                    <div class="form-group">

                        @include('layouts.error')

                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection