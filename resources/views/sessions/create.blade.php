@extends('layouts.master')

@section('content')

    <script>
        $( document ).ready(function() {
            $('.title').addClass('animated fadeInDown');
            $('.box').addClass('animated fadeInUp');
        });
    </script>

    <link href="./css/login.css" rel="stylesheet">

    <div class="container">
        <div class="row main">

            <!--PAGE HEADER-->
            <div class="panel-heading">
                <div class="panel-title text-center" style="position:relative; top:30px;">
                    <h1 class="title">Login to <strong>your</strong> Loughy Dogs account</h1>
                    <hr />
                </div>
            </div>

            <!--LOGIN BOX-->
            <div class="box">
                <!--LOGIN TITLE-->
                <div id="header">
                    <div id="cont-lock"><span class="lock"><i class="fa fa-lock"></i> Enter your information</span></div>
                </div>

                <!--LOGIN FORM/ INPUTS-->
                <form action="{{url('/login')}}" method="post">

                    {{csrf_field()}}

                    <!--USERNAME INPUT-->
                    <div class="group">
                        <div id="emailGroup">
                            <input class="inputMaterial" type="text" name="email" id="email" onkeyup="checkInput()">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>E-mail address</label>
                        </div>
                    </div>

                    <!--PASSWORD INPUT-->
                    <div class="group">
                        <div id="passwordGroup">
                            <input class="inputMaterial" type="password" name="password" id="password" onkeyup="checkInput()">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>Password</label>
                        </div>
                    </div>

                    <!--LOGIN BUTTON-->
                    <button id="buttonlogintoregister" name="submit" type="submit" onclick="return checkFields()">Login</button>

                    <div class="alert-danger error hidden">Wrong email or password entered</div>

                </form>
                <!--LOGIN FOOTER - REGISTER LINK-->
                <div id="footer-box">
                    <p class="footer-text">Not a member?<a href="{{url('/register')}}"><span class="sign-up"> Sign up now</span></a></p>
                </div>
            </div>

        </div>
    </div>


    <script>

        function checkFields(){
            var userName = document.getElementById("email").value;
            var passWord = document.getElementById("password").value;
            if(userName===""){
                $('#emailGroup').find(".bar").removeClass("bar").addClass("barError");
                $('.error').removeClass("hidden");
                return false;
            }else{
                return true;
            }

            if(passWord===""){
                $('#userName .bar').addClass('.barError');
                return false;
            }else{
                return true;
            }
        }


        function animateCss(animationName){
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            this.addClass('animated ' + animationName).one(animationEnd, function() {
                $(this).removeClass('animated ' + animationName);
            });
        }

        function checkInput(){
            var userName = document.getElementById("email").value;
            var passWord = document.getElementById("password").value;

            if(userName !== ""){
                $( "#emailGroup").find("label").addClass("inputHasContent");
            }else{
                $( "#emailGroup" ).find("label ").removeClass("inputHasContent");
            }

            if(passWord !== ""){
                $( "#passwordGroup").find("label").addClass("inputHasContent");
            }else{
                $( "#passwordGroup" ).find("label ").removeClass("inputHasContent");
            }
        }

    </script>

@endsection