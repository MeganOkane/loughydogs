@extends('layouts.master')

@section('pageTitle', 'About Us')

@section('content')

    <div class="aboutParallax">

        <h1 class="page_title">ABOUT US</h1>


    </div>

    <div class="container-fluid text-center">
        <h1 class="subhead">OUR STORY</h1>
        <p class="text-center"><em><strong>We love dogs as much as you do!</strong></em></p>
        <div class="container">
            <div class="row row-padding">
                <div class="col-sm-2">
                    <span class="story-icon"><img src="./img/icons/family-icon.png" alt="Family icon"></span>
                </div>

                <div class="col-sm-8">
                    <p class="text-center">
                        We are a family orientated business with a passion for dogs, we decided to take all our canine love and make the perfect facilities for a great doggy holiday/day care.
                </div>
            </div>

            <div class="row row-padding">
                <div class="col-sm-2">
                    <span class="story-icon"><img src="./img/icons/dog-house-icon.png" alt="Dog House icon"></span>
                </div>

                <div class="col-sm-8">
                        <p class="text-center">
                            We have purposely designed the kennels with your dog in mind; each chalet offers comfort, insulated roof, thermostat controlled heating, outside and inside spacious areas.
                            The sleeping area is approx a large 35 square foot and the outside runs are a massive 125 square foot, so you don't have to worry about your dog not having enough space to run about! We make sure the dogs are not in their chalets all the time, they are taken on regular walks and play time in our massive 200 sq metre exercise yard.
                        </p>
                </div>
            </div>

            <div class="row row-padding">
                <div class="col-sm-2">
                    <span class="story-icon"><img src="./img/icons/dog-icon.png" alt="Dog icon"></span>
                </div>

                <div class="col-sm-8">
                    <p class="text-center">
                        We ensure that each dog gets the attention, human contact and walks (guaranteed 2 per day) that they need. We create a special bond with each of our Loughy Lodgers and treat them as our own
                        and keep in mind that it's also the dogs holiday too!
                    </p>
                </div>
            </div>
        </div>

    </div>

    <div class="aboutus_parallax_1" style="height:550px;"></div>

    <div class="container-fluid text-center bg-grey">
        <h1 class="subhead">MEET THE TEAM</h1>
        <div class="container row-padding">
            <div class="row">
               <div class="col-sm-3 team-column-padding">
                    <div class="col-sm-12 thumbnail text-center">
                        <img onclick="showTeamMemberText('viv')" alt="Image of Viv" class="img-responsive" src=
                        "./img/viv.jpg">

                        <div class="caption">
                            <h4>Viv</h4>
                        </div>
                    </div>

                    <div id="viv" style="display: none">
                        <p class="meet-team-description">Viv is the founder of Loughy Dogs and has always had dogs in his life. He decided to design and build his own bespoke dog kennels to provide the best comfort and needs for doggys on their holidays!</p>
                    </div>

                </div>

                <div class="col-sm-3 team-column-padding">

                    <div class="col-sm-12 thumbnail text-center">
                        <img onclick="showTeamMemberText('megan')" alt="Image of Megan" class="img-responsive" src=
                        "./img/megan.jpg">

                        <div class="caption">
                            <h4>Megan</h4>
                        </div>
                    </div>

                    <div id="megan" style="display: none">
                        <p class="meet-team-description">Megan is one of our team members that constantly gives our dogs the walks and attention they need, she absolutely loves dogs and her job. As you can see she has a soft spot for our regular Shelby!</p>
                    </div>

                </div>

                <div class="col-sm-3 team-column-padding">

                    <div class="col-sm-12 thumbnail text-center">

                        <img onclick="showTeamMemberText('mark')" alt="" class="img-responsive" src=
                        "./img/mark.jpg">
                        <div class="caption">
                            <h4>Mark</h4>
                        </div>
                    </div>

                    <div id="mark" style="display: none">
                        <p class="meet-team-description">Mark is our top dog! He loves to cuddle, walk and play with all our visitors. Mark also enjoys taking professional pictures of our Loughy Lodger's. </p>
                    </div>

                </div>

                <div class="col-sm-3">

                    <div class="col-sm-12 thumbnail text-center">
                        <img onclick="showTeamMemberText('marley')" alt="" class="img-responsive" src=
                        "./img/marleyinmud.jpg">

                        <div class="caption">
                            <h4>Marley</h4>
                        </div>
                    </div>

                    <div id="marley" style="display: none">
                        <p class="meet-team-description">Marley is our top doggy greeter, he loves making new friends with all our visitors and enjoys digging up our garden!</p>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="aboutus_parallax_2" style="height:550px;"></div>

    <div class="container-fluid text-center bg-grey">
        <h1 class="subhead">OUR LOCATION</h1>
        <div class="row row-padding">
            <div class="col-sm-8">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2303.7692035596156!2d-6.521672784373558!3d54.73127438029257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48605a5ad5a1485b%3A0xca19fd150f42a3ed!2s28+Lough+Rd%2C+Magherafelt+BT45+6LN!5e0!3m2!1sen!2suk!4v1496447437372" style="border:0px;" class="googleMap"></iframe>
            </div>
                <div class="col-sm-4">
                    <span class="story-icon"><img class="location-icon" src="./img/icons/location-icon.png" alt="Location icon"></span>
                    <div>
                         <p class="text-left">
                            Loughy Dogs are based in Mid Ulster, Northern Ireland , right beside the beautiful Lough Neagh. If you are thinking of jetting off on holidays we are only 20 minutes from Belfast International Airport and 45 mins from Belfast City Airport!  We are close to the following places:
                        </p>
                    </div>

                    <div class="text-left">
                        <ul>
                            <li>Magherafelt</li>
                            <li>Toomebridge</li>
                            <li>Cookstown</li>
                            <li>Maghera</li>
                            <li>North West Area (on the way to the airport)</li>
                        </ul>
                    </div>

                    <a href="contactus"><button type="button" class="btn btn-loughy btn-lg"> Get in touch </button></a>
                </div>
        </div>
    </div>


    <script>
        function showTeamMemberText(id) {
            var teamCaption = document.getElementById(id);
                if (teamCaption.style.display === "none") {
                    teamCaption.style.display = "block";
                } else {
                    teamCaption.style.display = "none";
                }
        }
    </script>

@endsection