                                                                                                   @extends('layouts.master')

@section('pageTitle', 'Home')

@section('content')

    <div class="parallax indexImg"><img id="bigLogo" src="{{asset('img/loughylogo.png')}}" alt="Loughy Dogs Logo"/></div>

    <script>
        $( document ).ready(function() {
            $('#bigLogo').addClass('animated fadeInUp');
        });
    </script>

    <div class="container-fluid text-center">

        <br>

        <div class="col-md-8 who-we-are">
            <h1 class="subhead">WHAT WE DO</h1>
            <br>
            <p>We are DAERA registered dog boarding kennels situated in Magherafelt, Mid Ulster, Northern Ireland that people trust with their beloved furry friends. We cater to surrounding areas such as Cookstown,
                Maghera, Toome, Randalstown and North West.</p>

            <p>Your dog will spend their days socialising with friends in our massive exercise play yard, enjoying walks around the beautiful Lough Neagh and end their day sleeping in
                in our heated chalets!</p>

            <div class="col-md-4 contact-button">
                <a href="contactus"><button type="button" class="btn btn-loughy btn-lg"> Get in touch </button></a>

            </div>
        </div>

        <div class="col-md-2">
            <img src="{{asset('img/dogbylough.jpg')}}" class="overview-img" alt="Image of Dog by Lough Neagh"/>
        </div>
    </div>

    <div class="home_parallax_4" style="height:550px;"></div>

    <div class="container-fluid text-center bg-grey">
        <h1 class="subhead">WHY PEOPLE CHOOSE US</h1>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <span class="glyphicon logo-small"><img src="./img/icons/hugearea.png" alt="Huge Space icon"></span>
                <h4>Huge Space</h4>
                <p>Our facilities are like no other! The dog's large heated sleeping area is 35 square foot and the outside runs are 125 square foot plus our massive exercise play yard (200 sq metre)!</p>
            </div>
            <div class="col-sm-4">
                <span class="glyphicon logo-small"><img src="./img/icons/camera-icon.png"></span>
                <h4>Regular Updates</h4>
                <p>We ensure that our customer's get regular picture and video updates of their dog's on walks and playing with others.(Our customer's love this!)</p>
            </div>
            <div class="col-sm-4">
                <span class="glyphicon logo-small"><img src="./img/icons/clock-icon.png" alt="24/7 Vet icon"></span>
                <h4>24/7 Vet</h4>
                <p class="webLink">We have a 24/7 vet on call for emergencies, why not check them out at <a class="webLink" href ="http://drumraineyvets.co.uk/">Drumrainey Vets</a>. Loughy Dogs customers get 10% off their dog's vaccinations!</p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <span class="glyphicon logo-small"><img src="./img/icons/eye1.png" alt="See for yourself icon"></span>
                <h4>See for yourself</h4>
                <p>We are encourage people to come visit our facilities during our busiest times and see us in action, so why not check us out! </p>
            </div>
            <div class="col-sm-4">
                <span class="glyphicon logo-small"><img src="./img/icons/pawicon.png" alt="Breed Friendly icon"></span>
                <h4>Breed Friendly</h4>
                <p>We don't discriminate against dog breeds or sizes, here at Loughy Dogs we accept all kinds of dogs and ensure we have the facilities to keep them occupied.</p>
            </div>
            <div class="col-sm-4">
                <span class="glyphicon logo-small"><img src="./img/icons/trophy.png" alt="Hard Working icon"></span>
                <h4>Hard Working</h4>
                <p>Our jobs are never done, we are constantly ensuring your dogs are looked after through walks, cuddling, exercise and much more!</p>
            </div>
        </div>
    </div>

    <div class="home_parallax_1" style="height:550px;"></div>

    <div class="container-fluid text-center">
        <h1 class="subhead">OUR LOUGHY LODGERS</h1>
        <br>
        <br>

        <div class="container row-padding">
            <div class=" col-sm-12 row">
                <div class="col-sm-4">
                    <div class="col-sm-12 thumbnail text-center">
                        <img alt="Image of Shelby" class="img-responsive" src=
                        "./img/loughylodgers/shelbycloseup.jpg">

                        <div class="caption">
                            <h4>Shelby</h4>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="col-sm-12 thumbnail text-center">
                        <img alt="Image of Maisey" class="img-responsive" src=
                        "./img/loughylodgers/maisey.jpg">

                        <div class="caption">
                            <h4>Maisey</h4>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="col-sm-12 thumbnail text-center">
                        <img alt="Image of Tilly" class="img-responsive" src=
                        "./img/loughylodgers/tilly.jpg">

                        <div class="caption">
                            <h4>Tilly</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class=" col-sm-12 row">
                <div class="col-sm-4">
                    <div class="col-sm-12 thumbnail text-center">
                        <img alt="Image of Tara" class="img-responsive" src=
                        "./img/loughylodgers/tara.jpg">

                        <div class="caption">
                            <h4>Tara</h4>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="col-sm-12 thumbnail text-center">
                        <img alt="Image of Holly" class="img-responsive" src=
                        "./img/loughylodgers/holly.jpg">

                        <div class="caption">
                            <h4>Holly</h4>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="col-sm-12 thumbnail text-center">
                        <img alt="Image of Ben" class="img-responsive" src=
                        "./img/loughylodgers/ben.jpg">

                        <div class="caption">
                            <h4>Ben</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <a href="gallery" class="btn btn-default btn-lg">See Gallery</a>
    </div>


    <div class="home_parallax_2" style="height:550px;"></div>

    <!--Customer review section-->
    <div class="container-fluid text-center bg-grey">
        <h1 class="subhead">WHAT OUR CUSTOMERS SAY</h1>
        <p class="text-center webLink"><em><strong>Check out the rest of our brilliant reviews on our Facebook Page <a class="webLink" href="https://www.facebook.com/loughydogs/reviews">Loughy Dogs</a></strong></em></p>
        <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
                <li data-target="#myCarousel" data-slide-to="4"></li>
                <li data-target="#myCarousel" data-slide-to="5"></li>
                <li data-target="#myCarousel" data-slide-to="6"></li>
                <li data-target="#myCarousel" data-slide-to="7"></li>
                <li data-target="#myCarousel" data-slide-to="8"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">

                <div class="item active">
                    <h4>"Left my pug Doug here for the weekend , and was just perfect for him had loads off walks and fun on his holidays , we can not recommend this place enough and have already him booked in again for his holidays in the summer. Thanks again Viv"<br><span style="font-style:normal;">Geoff Booth, Magherafelt</span></h4>
                </div>

                <div class="item">
                    <h4>"Drove from Newcastle to leave Ruby at Loughy Dogs, Ruby had never spent a night without me before (she is almost 6 years old). I was very nervous about leaving her but I definitely made the right choice bringing Ruby here. The kennels are amazing, very well fitted out, spotlessly clean and every detail noted on the record sheet. "<br><span style="font-style:normal;">Kathryn Stuart, Newcastle Co.Down</span></h4>
                </div>

                <div class="item">
                    <h4>"Was looking somewhere quieter and smaller so my dog could have that extra personal touch like a walk each day or a belly rub now and again. I found exactly what I was looking for with Viv at loughy dogs."<br><span style="font-style:normal;">Isobel Loughan, Antrim</span></h4>
                </div>

                <div class="item">
                    <h4>"I recently had my Golden retriever Tara staying at these kennels and can truthfully say it was by far the best experience we have had with kennels
                        Tara was treated like a member of the family and even became best friends with the owners dog."<br><span style="font-style:normal;">Roddy Keenan, Magherafelt</span></h4>
                </div>

                <div class="item">
                    <h4>"We left our energetic Doberman here for a few days recently. We live in Omagh but had heard great reports about Loughy Dogs, and the fact that is on your way to the airport - very handy! Great big runs and kennels, kept very clean and the dogs are obviously treated like another member of the family"<br><span style="font-style:normal;">Sharon Harvey, Omagh</span></h4>
                </div>

                <div class="item">
                    <h4>"The dogs get a chance to socialise with other dogs which is so so good for them and they get walked so much they are just loved from the minute you drop them off. I would honestly say I have never seen anything like the set up at Loughy Dog's."<br><span style="font-style:normal;">Angela Ross, Ballymena</span></h4>
                </div>

                <div class="item">
                    <h4>"Fantastic place. My dog Bamberix he love it. Dog was in very good hands. After one week in great form. Viv sent me many pictures when he walked with dog and played with him."<br><span style="font-style:normal;">Rafał Kowalski , Magherafelt</span></h4>
                </div>

                <div class="item">
                    <h4>"Fantastic. Viv made me feel so happy about leaving Molly. The facilities are great with outside play area and beautiful walks. Daily photographic updates of a very happy dog. Was actually worried that she wouldn't want to come home. Totally recommend it."<br><span style="font-style:normal;">Helen Mc Toal, Newbridge</span></h4>
                </div>

                <div class="item">
                    <h4>"We left our two dogs with Viv at Loughy Dogs. We had heard great reports about Loughy Dogs but we didn't realise just how great this set up is here. The runs are spacious and spotless clean, there is a big yard for socialization if you want and Viv kept us updated throughout their stay which was very reassuring for us."<br><span style="font-style:normal;">Kevin Scullion, Draperstown</span></h4>
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>



    </div>

    <div class="home_parallax_3" style="height:550px;"></div>

    <div id="contactus" class="container-fluid text-center">
        <h1 class="subhead">CONTACT US</h1>
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="row text-left">
            <div class="container">
                <div class="col-sm-5">
                    <p>Contact us and we'll get back to you within 24 hours.</p>
                    <p><span class="glyphicon glyphicon-map-marker blue"></span> 28 Lough Road, Magherafelt</p>
                    <p><span class="glyphicon glyphicon-phone blue"></span> 07771994422</p>
                    <p><span class="glyphicon glyphicon-envelope blue"></span> info@loughydogs.com</p>
                </div>
                <div class="col-sm-7">

                    <form method="POST" action="{{url('home/post')}}">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-xs-6 form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            </div>
                            <div class="col-xs-6 form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <input class="form-control" id="subject" name="subject" placeholder="Subject" type="text" required>
                                <span class="text-danger">{{ $errors->first('subject') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group {{ $errors->has('message') ? 'has-error' : '' }}">
                                <textarea class="form-control" id="message" name="message" placeholder="Comment" rows="5"></textarea><br>
                                <span class="text-danger">{{ $errors->first('message') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group contact-submit">
                                <button class="btn btn-success" type="submit">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection