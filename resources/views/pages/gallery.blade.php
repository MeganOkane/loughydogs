@extends('layouts.master')

@section('pageTitle', 'Gallery')

@section('content')

    <div class="galleryParallax">

        <h1 class="page_title">OUR GALLERY</h1>

    </div>

    <div class="container-fluid text-center">
        <h1 class="subhead">LOUGHY LODGERS</h1>
        <p class="text-center"><em><strong>We love each of our Loughy Lodgers and want to share their happiness with you!</strong></em></p>

        <div class="row gallery-row-padding">
            <div class="column">
                <img id="1" data-toggle="modal" data-target="#myModal"  src="./img/galleryimgs/taraingarden.jpg" alt="Image of Tara in the garden" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="2" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/betsy.jpg" alt="Image of Betsy" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="3" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/millieplaying.jpg" alt="Image of Millie Playing" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="4" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/bailey.jpg" alt="Image of Bailey" style="width:100%" class="hover-shadow cursor">
            </div>
        </div>

        <div class="row gallery-row-padding">
            <div class="column">
                <img id="5" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/max2.jpg" alt="Image of Ku" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="6" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/doug2.jpg" alt="Image of Doug" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="7" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/willie2.jpg" alt="Image of Willie" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="8" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/vivwithdogs.jpg" alt="Image of a Loughy Lodger" style="width:100%" class="hover-shadow cursor">
            </div>
        </div>

        <div class="row gallery-row-padding">
            <div class="column">
                <img  id="10" data-toggle="modal" data-target="#myModal"src="./img/galleryimgs/bamber.jpg" alt="Image of Bamber" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img  id="11" data-toggle="modal" data-target="#myModal"src="./img/galleryimgs/mitzy.jpg" alt="Image of Mitzy" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img  id="12" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/ozzy.jpg" alt="Image of Ozzy" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="13" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/oscarrottweiler.jpg" alt="Image of Oscar" style="width:100%" class="hover-shadow cursor">
            </div>
        </div>


        <div class="row">
            <div class="column">
                <img  id="10" data-toggle="modal" data-target="#myModal"src="./img/galleryimgs/shelby4.jpg" alt="Image of Bamber" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img  id="11" data-toggle="modal" data-target="#myModal"src="./img/galleryimgs/littledogs.jpg" alt="Image of Mitzy" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img  id="12" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/blacklab.jpg" alt="Image of Ozzy" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="13" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/nala2.jpg" alt="Image of Oscar" style="width:100%" class="hover-shadow cursor">
            </div>
        </div>
        <br>
        <br>
        <button id="showButton" class="btn btn-default btn-lg" onclick="showMoreGallery()"><span>See More</span></button>

    </div>

    <div id="showMoreGallery" style="display: none;" class="container-fluid-gallery text-center">

        <div class="row gallery-row-padding">
            <div class="column">
                <img id="14" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/oscar2.jpg" alt="Image of Oscar" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="15" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/millieandozzy.jpg" alt="Image of Millie and Ozzy" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="16" data-toggle="modal" data-target="#myModal"  src="./img/galleryimgs/shitzu2.jpg" alt="Image of a Loughy Lodger" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="17" data-toggle="modal" data-target="#myModal"  src="./img/galleryimgs/benandchico1.jpg" alt="Image of Ben and Chico" style="width:100%" class="hover-shadow cursor">
            </div>
        </div>

        <div class="row gallery-row-padding">
            <div class="column">
                <img id="18" data-toggle="modal" data-target="#myModal"  src="./img/galleryimgs/browndog2.jpg" alt="Image of a Loughy Lodger" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="19" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/charlie.jpg" alt="Image of Charlie" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="20" data-toggle="modal" data-target="#myModal"  src="./img/galleryimgs/doug3.jpg" alt="Image of Doug" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="21" data-toggle="modal" data-target="#myModal"  src="./img/galleryimgs/sparky.jpg" alt="Image of Sparky" style="width:100%" class="hover-shadow cursor">
            </div>
        </div>

        <div class="row gallery-row-padding">
            <div class="column">
                <img id="22" data-toggle="modal" data-target="#myModal"  src="./img/galleryimgs/millie2.jpg" alt="Image of Millie" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="23" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/rua2.jpg" alt="Image of Rua" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="24" data-toggle="modal" data-target="#myModal"  src="./img/galleryimgs/shylo.jpg" alt="Image of Shylo" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="25" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/spaniel2.jpg" alt="Image of a Loughy Lodger" style="width:100%" class="hover-shadow cursor">
            </div>
        </div>


        <div class="row gallery-row-padding">
            <div class="column">
                <img id="22" data-toggle="modal" data-target="#myModal"  src="./img/galleryimgs/cara.jpg" alt="Image of Cara" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="23" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/dogingrass.jpg" alt="Image of Loughy Dogs" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="24" data-toggle="modal" data-target="#myModal"  src="./img/galleryimgs/echo.jpg" alt="Image of Echo" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="25" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/jojo.jpg" alt="Image of a Jojo" style="width:100%" class="hover-shadow cursor">
            </div>
        </div>


        <div class="row">
            <div class="column">
                <img id="22" data-toggle="modal" data-target="#myModal"  src="./img/galleryimgs/lucy.jpg" alt="Image of Lucy" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="23" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/max.jpg" alt="Image of Max" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="24" data-toggle="modal" data-target="#myModal"  src="./img/galleryimgs/dogsinyard.jpg" alt="Image of Loughy Lodgers" style="width:100%" class="hover-shadow cursor">
            </div>
            <div class="column">
                <img id="25" data-toggle="modal" data-target="#myModal" src="./img/galleryimgs/molly.jpg" alt="Image of Molly" style="width:100%" class="hover-shadow cursor">
            </div>
        </div>
    </div>

    <div>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <img class="img-responsive" src="" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="gallery_parallax_1" style="height:550px;"></div>

    <div class="container-fluid text-center bg-grey">
        <h1 class="subhead">LOUGHY DOGS FACILITIES</h1>
        <p class="text-center"><em><strong>We take pride in our kennels and want you to see what we have to offer!</strong></em></p>
        <br>
        <br>

        <div class="container row-padding">
            <div class=" col-sm-12 row">
                <div class="col-sm-6 kennel-img-col-padding">
                    <div class="col-sm-12 thumbnail">
                        <img id="34" data-toggle="modal" data-target="#myModal"  src="./img/kennels/kennelrun1.jpg" alt="Image of a run in a Loughy Dogs kennel">
                    </div>
                </div>

                <div class="col-sm-6 kennel-img-col-padding">
                    <div class="col-sm-12 thumbnail">
                        <img id="35" data-toggle="modal" data-target="#myModal"  src="./img/kennels/kennelrun2.jpg" alt="Image of a run in a Loughy Dogs kennel">
                    </div>
                </div>
            </div>
        </div>


        <div class="container row-padding">
            <div class=" col-sm-12 row">
                <div class="col-sm-6 kennel-img-col-padding">
                    <div class="col-sm-12 thumbnail text-center">
                        <img id="36" data-toggle="modal" data-target="#myModal" src="./img/kennels/kennelrun3.jpg" alt="Image of a run in a Loughy Dogs kennel">
                    </div>
                </div>

                <div class="col-sm-6 kennel-img-col-padding">
                    <div class="col-sm-12 thumbnail text-center">
                        <img id="37" data-toggle="modal" data-target="#myModal"  src="./img/kennels/kennelrun4.jpg" alt="Image of a run in a Loughy Dogs kennel">
                    </div>
                </div>
            </div>
        </div>


        <div class="container row-padding">
            <div class=" col-sm-12 row">
                <div class="col-sm-6 kennel-img-col-padding">
                    <div class="col-sm-12 thumbnail">
                        <img id="26" data-toggle="modal" data-target="#myModal" src="./img/kennels/receptionfront.jpg" alt="Image of Loughy Dogs reception">
                    </div>
                </div>

                <div class="col-sm-6 kennel-img-col-padding">
                    <div class="col-sm-12 thumbnail">
                        <img id="27" data-toggle="modal" data-target="#myModal" src="./img/kennels/exerciseyard2.jpg" alt="Image of Loughy Dogs exercise yard">
                    </div>
                </div>
            </div>
        </div>


        <div class="container row-padding">
            <div class=" col-sm-12 row">
                <div class="col-sm-6 kennel-img-col-padding">
                    <div class="col-sm-12 thumbnail text-center">
                        <img id="28" data-toggle="modal" data-target="#myModal" src="./img/kennels/run1.jpg" alt="Image of a run in a Loughy Dogs kennel">
                    </div>
                </div>

                <div class="col-sm-6 kennel-img-col-padding">
                    <div class="col-sm-12 thumbnail text-center">
                        <img id="29" data-toggle="modal" data-target="#myModal"  src="./img/kennels/run3.jpg" alt="Image of a run in a Loughy Dogs kennel">
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <button id="showMore" class="btn btn-default btn-lg" onclick="showMoreFacilities()"><span>See More</span></button>
    </div>

    <div id="showMoreFacilities" style="display: none;" class="container-fluid-gallery text-center bg-grey">

        <div class="container row-padding">
            <div class=" col-sm-12 row">
                <div class="col-sm-6 kennel-img-col-padding">
                    <div class="col-sm-12 thumbnail text-center">
                        <img id="30" data-toggle="modal" data-target="#myModal" src="./img/kennels/insidekennels.jpg" alt="Image of inside of a Loughy Dogs kennel">
                    </div>
                </div>

                <div class="col-sm-6 kennel-img-col-padding">
                    <div class="col-sm-12 thumbnail text-center">
                        <img id="31" data-toggle="modal" data-target="#myModal"  src="./img/kennels/kennel.jpg" alt="Image of a Loughy Dogs kennel">
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class=" col-sm-12 row">
                <div class="col-sm-6 kennel-img-col-padding">
                    <div class="col-sm-12 thumbnail text-center">
                        <img id="32" data-toggle="modal" data-target="#myModal" src="./img/kennels/exercise3.jpg" alt="Image of Loughy Dogs exercise yard">
                    </div>
                </div>

                <div class="col-sm-6 kennel-img-col-padding">
                    <div class="col-sm-12 thumbnail text-center">
                        <img d="33" data-toggle="modal" data-target="#myModal"  src="./img/kennels/loughneagh.jpg" alt="Image of Lough Neagh near the Loughy Dogs facilities">
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="gallery_parallax_2" style="height:550px;"></div>

    <!-- Video Section -->
    <div class="container-fluid text-center">
        <h1 class="subhead">LOUGHY DOGS VIDEOS</h1>
        <p class="text-center"><em><strong>Check out our Loughy Lodgers in action!</strong></em></p>

        <video controls poster="./img/kennels/loughneagh.jpg" style="width:80%;">
            <source src="./img/chicoben.mp4" type="video/mp4">
            Your browser does not support HTML5 video.
        </video>

    </div>


    <script>

        $(document).ready(function () {
            $('#myModal').on('show.bs.modal', function (e) {
                var image = $(e.relatedTarget).attr('src');
                $(".img-responsive").attr("src", image);
            });
        });

            function showMoreGallery() {
                var elem = document.getElementById("showButton");
                if (elem.innerText === "See More") {
                    elem.innerHTML = "See Less";
                    document.getElementById("showMoreGallery").style.display = 'block';
                }else {
                    elem.innerHTML = "See More";
                    document.getElementById("showMoreGallery").style.display = 'none';
                }
            }

        function showMoreFacilities() {
            var elem = document.getElementById("showMore");
            if (elem.innerText === "See More") {
                elem.innerHTML = "See Less";
                document.getElementById("showMoreFacilities").style.display = 'block';
            }else {
                elem.innerHTML = "See More";
                document.getElementById("showMoreFacilities").style.display = 'none';
            }
        }
    </script>

@endsection