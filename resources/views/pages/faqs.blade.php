@extends('layouts.master')

@section('pageTitle', 'Frequently Asked Questions')

@section('content')

    <div class="faqParallax">
        {{--<div class="row">--}}

            {{--<div class="col-lg-12">--}}

                <h1 class="page_title">FAQ'S</h1>

            </div>

        </div>

    </div>

    <div class="container-fluid text-center">
        <h1 class="subhead">FEQUENTLY ASKED QUESTIONS</h1>
        <p class="text-center webLink"> <strong>Has your question not been answered? Then feel free to get in touch with us at our <a href="{{url('/contactus')}}" >Contact</a> page.</strong></p>


        <button class="accordion">Q: What vaccinations does my dog require before staying at Loughy Dogs?</button>
        <div class="panel answer">
            <p>A: We have a strict policy that all our dogs must have their yearly vaccinations up to date and the kennel cough vaccine ( at least 2 weeks before their stay) before staying at Loughy Dog's (proof is required), we want all our little friends to be healthy and safe!</p>
        </div>

        <button class="accordion">Q: What if there is an emergency with my dog/s?</button>
        <div class="panel answer webLink">
            <p>A: Having assessed the situation and the degree of emergency we will either call you and/or our vet which we have on call 24/7(situated 10 minutes away). You will be responsible for any vet expenses incurred.
            Why not checkout our 24/7 vets at <a href ="http://drumraineyvets.co.uk/">Drumrainey Vets</a> , each Loughy Dogs customer gets 10% off their set of vaccinations!</p>
        </div>

        <button class="accordion">Q: What if my dog doesn’t mix well with the other dogs? </button>
        <div class="panel answer">
            <p>A: It's normal for every dog to feel nervous when they first come, we introduce each dog slowly to a small groups of the other dogs and if they still aren't comfortable we ensure they still get time in our exercise yard, walks and loads of human contact.</p>
        </div>

        <button class="accordion">Q: Does my dog need to be obedient to stay? </button>
        <div class="panel answer">
            <p>A: No. It does help if your dog has at least basic obedience training but often they learn by copying the example of the other dogs at Loughy Dog's.</p>
        </div>

        <button class="accordion">Q: Do you only offer dog boarding? </button>
        <div class="panel answer">
            <p>A: No, we offer many services such as doggy daycare, half daycare and doggy pickup and return (includes airport pickups/drop offs). Check out our Services page for more info!</p>
        </div>

        <button class="accordion">Q: What are your opening hours? </button>
        <div class="panel answer">
            <p>A: Monday to Friday: 10:00 - 19:00 <br>
                A: Saturday: 10:00 - 17:00<br>
                A: Sunday: 11:00 - 14:00
            </p>
        </div>

    </div>

    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight){
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }
    </script>



@endsection