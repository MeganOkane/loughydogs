@extends('layouts.master')

@section('pageTitle', 'Contact Us')

@section('content')

    <main class="container main-body">

        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <div class="row">

            <section class="contact-details col-lg-6">

                <h2 class="contact-heading white">Contact Details</h2>

                <div class="row">

                    <div class="row-centered">
                        <div class="col-lg-5"><i class="fa fa-phone contact-icon grey" aria-hidden="true"></i></div>
                        <div class="col-lg-7"><span class="contact-detail white contact-phone-number">07771 994422</span></div>
                    </div>


                </div>

                <div class="row">

                    <div class="row-centered">
                        <div class="col-lg-5"><i class="fa fa-envelope contact-icon grey" aria-hidden="true"></i></div>
                        <div class="col-lg-7"><span class="contact-detail white">info@loughydogs.com</span></div>
                    </div>


                </div>

                <div class="row">

                    <div class="row-centered">
                        <div class="col-lg-5"><i class="fa fa-map-marker contact-icon grey" style="padding-right:5px;" aria-hidden="true"></i></div>
                        <div class="col-lg-7"><span class="contact-detail white">28 Loughy Road, Magherafelt</span></div>
                    </div>


                </div>

            </section>

            <section class="contact-questions col-lg-6">

                <h2 class="contact-heading blue">Find us on Social Media!</h2>

                <div class="row">

                    <div class="row-centered">
                        <a href="http://www.facebook.com/loughydogs"><div class="col-lg-4"><i class="fa fa-facebook-official contact-icon blue" aria-hidden="true"></i></div></a>
                        <div class="col-lg-8"><span class="contact-detail white social-media-link"> <a href="http://www.facebook.com/loughydogs"> facebook.com/loughydogs</a></span></div>
                    </div>

                </div>

                <div class="row">

                    <div class="row-centered">
                        <a href="http://www.instragram.com/loughydogs"><div class="col-lg-4"><i class="fa fa-instagram contact-icon blue" aria-hidden="true"></i></div></a>
                        <div class="col-lg-8"><span class="contact-detail white social-media-link"> <a href="http://www.instragram.com/loughydogs"> instagram.com/loughydogs</a></span></div>
                    </div>

                </div>

                <div class="row">

                    <div class="row-centered">
                        <a href="http://www.twitter.com/loughydogs"><div class="col-lg-4"><i class="fa fa-twitter-square contact-icon blue" aria-hidden="true"></i></div></a>
                        <div class="col-lg-8"><span class="contact-detail white social-media-link"><a href="http://www.twitter.com/loughydogs">twitter.com/loughydogs</a></span></div>
                    </div>

                </div>

            </section>

        </div>

        <div class="row">

            <section class="contact-askquestion col-lg-12">

                <h2 class="contact-heading white">Have a question?</h2>

                <p class="contact-intro faqLink"> Check out our <strong><a href="{{url('/faqs')}}" >Frequently Asked Questions </a></strong> page to see if we have already answered your question. If not don't worry! Contact us
                    via one of the contact details or social media links above. You can also contact via email using the contact form below. We would would be happy to answer your questions! </p>

                <div class="container">

                    <form class="form-horizontal" method="POST" action="{{url('contact')}}">
                        {{ csrf_field() }}
                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label class="contact-label">Your Name:</label>
                                    <input type="text" class="form-control contact-input" id="name" placeholder="Your Name" name="name" required />
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <label class="contact-label">Your E-mail:</label>
                                    <input type="text" class="form-control contact-input" id="email" placeholder="Enter Email" name="email" required />
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
                                    <label class="contact-label">Message Subject:</label>
                                    <input type="text" class="form-control contact-input" id="subject" placeholder="Message Subject" name="subject" required />
                                    <span class="text-danger">{{ $errors->first('subject') }}</span>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
                                    <label class="contact-label">Message:</label>
                                    <textarea class="form-control contact-input" id="message" placeholder="Enter Message" name="message" required></textarea>
                                    <span class="text-danger">{{ $errors->first('message') }}</span>
                                </div>

                            </div>

                        </div>


                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group contact-submit">
                                    <button class="btn btn-success btn-lg">Submit</button>
                                </div>

                            </div>

                        </div>

                    </form>
                </div>


            </section>

        </div>

        <div class="row">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2303.7692035596156!2d-6.521672784373558!3d54.73127438029257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48605a5ad5a1485b%3A0xca19fd150f42a3ed!2s28+Lough+Rd%2C+Magherafelt+BT45+6LN!5e0!3m2!1sen!2suk!4v1496447437372" style="border:0px" class="googleMap"></iframe>

        </div>

    </main>

@endsection