@extends('layouts.master')

@section('pageTitle', 'Page not found')

@section('content')

    <div class="container main-body text-center">

        <h1>404 - Page not found!</h1>

        <object type="image/svg+xml" data="img/icons/404.svg" style="width:25vw;">
            Your browser does not support SVG
        </object>


        <h2>Sorry, the page you are looking for cannot be found.</h2>

    </div>

@endsection