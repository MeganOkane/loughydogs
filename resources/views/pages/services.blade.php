@extends('layouts.master')

@section('pageTitle', 'Our Services')

@section('content')

    <div class="servicesParallax">

        <h1 class="page_title">OUR SERVICES</h1>

    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <a href="#doggydaycare" class="blue">
                    <div class="col-md-3">
                        <img src="{{asset('img/icons/fullDayIcon.png')}}" class="icon" alt="Full Doggy Day Care icon"/>
                        <div class="middle">
                            <div class="text">Learn More</div>
                        </div>
                        <p class="text-center service_name">Day Care</p>
                    </div>
                </a>
                <a href="#halfdaycare" class="blue">
                    <div class="col-md-3 serviceIcon">
                        <img src="{{asset('img/icons/halfDayIcon.png')}}" class="icon" alt="Half Doggy Day Care icon"/>
                        <div class="middle">
                            <div class="text">Learn More</div>
                        </div>
                        <p class="text-center service_name">Half Day Care</p>
                    </div>
                </a>
                <a href="#dogboarding" class="blue">
                    <div class="col-md-3 serviceIcon">
                        <img src="{{asset('img/icons/kennelIcon.png')}}" class="icon" alt="Boarding icon" />
                        <div class="middle">
                            <div class="text">Learn More</div>
                        </div>
                        <p class="text-center service_name">Boarding</p>
                    </div>
                </a>
                <a href="#airportpickup" class="blue">
                <div class="col-md-3 serviceIcon">
                    <img src="{{asset('img/icons/airportIcon.png')}}" class="icon" alt="Airport pickup icon"/>
                    <div class="middle">
                        <div class="text">Learn More</div>
                    </div>
                    <p class="text-center service_name">Airport Pickups</p>
                </div>
                </a>
            </div>
        </div>
    </div>

    <div class="services_parallax_1" style="height:550px;"></div>

    <div id="doggydaycare" class="container-fluid bg-grey">

        <div class="row">

            <div class="col-md-8">
                <h1 class="subhead text-center">DOGGY DAY CARE</h1>

                <p class="service-text">
                    Your dog will spend their day playing, going for walks and relaxing with their other Loughy Lodger friends! Your Loughy Dog can make use of our massive exercise yard or spend some down time in their large & private 125sq ft run and 35 sq ft sleeping area.
                </p>

                <p class="service-text">
                    We don't discriminate! We welcome dogs of all breeds, sizes and ages. We do everything to satisfy your dog's needs.
                </p>

                <p class="service-text">
                    All our dog sleeping areas are heated and shielded from the elements to ensure your dog can snuggle up warm!
                </p>

                <div class="col-md-4 contact-button">
                <a href="contactus"><button type="button" class="btn btn-loughy btn-lg"> Get in touch </button></a>

                </div>

            </div>


            <div class="col-md-2">

                <img src="{{asset('img/TheLough.jpg')}}" class="square-img" alt="Image of Lough Neagh near Loughy Dogs"/>

            </div>
        </div>
    </div>

    <div class="services_parallax_2" style="height:550px;"></div>

    <div id="halfdaycare" class="container-fluid bg-grey">
        <div class="row">

            <div class="col-md-4">

                <img src="{{asset('img/shelby_1.jpg')}}" class="square-img" alt="Image of Shelby"/>

            </div>

            <div class="col-md-8">

                <h1 class="subhead text-center">HALF DAY CARE</h1>

                <div class="container">

                    <div class="row">

                        <div class="col-md-8 col-lg-offset-1">

                            <p class="service-text">
                                Your dog will get to spend their morning or afternoon at Loughy Dogs! Your dog will receive the usual VIP treatment during the half day at Loughy Dogs. This gives your dog the opportunity to make full use of all our facilities,
                                socialise with their friends, exercise and relax.
                            </p>

                            <p class="service-text">
                                Not only can your dog relax, but so can you! You will receive your usual daily updates and pictures of your dog having fun at Loughy Dogs!
                            </p>


                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="contactus"><button type="button" class="btn btn-loughy btn-lg"> Get in touch </button></a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="services_parallax_3" style="height:550px;"></div>

    <div id="dogboarding" class="container-fluid bg-grey">
        <div class="row">

            <div class="col-md-8">
                <h1 class="subhead text-center">DOG BOARDING</h1>

                <p class="service-text">
                    Don't just send yourself on holiday, send your dog on one too! You can relax while your away knowing that your dog is getting all the love and attention they deserve.
                </p>

                <p class="service-text">
                    Your dog will get to make use out of our large exercise yard, go on walks, socialise and relax everyday! You will also receive daily updates and photos of your dog having the time of their lives!
                </p>

                <p class="service-text">
                    We are a family orientated business who owns a dog (the lovely Marley!) too. This makes us want to make sure that your dog is looked after to a very high standard and strive to treat them as our own. We know sending your dog to a kennel can be scary, so why not come out and visit us to ease the worries? Contact us today!
                </p>

                <div class="col-md-4 contact-button">
                    <a href="contactus"><button type="button" class="btn btn-loughy btn-lg"> Get in touch </button></a>
                </div>

            </div>

            <div class="col-md-2">
                <img src="{{asset('img/maisie.jpg')}}" class="square-img" alt="Image of Maisie"/>
            </div>
        </div>
    </div>

    <div class="services_parallax_4" style="height:550px;"></div>

    <div id="airportpickup" class="container-fluid bg-grey">
        <div class="row">

            <div class="col-md-4">

                <img src="{{asset('img/Tara.jpg')}}" class="square-img" alt="Image of Tara"/>

            </div>

            <div class="col-md-8">

                <h1 class="subhead text-center">AIRPORT PICKUP</h1>

                <div class="container">

                    <div class="row">

                        <div class="col-md-8 col-lg-offset-1">

                            <p class="service-text">
                                Going on holidays can be stressful, so why not make it less stressful by not worrying about having to drop your dog of at dog boarding before your leave?
                            </p>

                            <p class="service-text">
                                Your dog can be picked up by Loughy Dogs at the airport. We will be waiting at the airport to collect your Loughy Dog to bring them on their holidays while you go on yours!
                            </p>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <a href="contactus"><button type="button" class="btn btn-loughy btn-lg"> Get in touch </button></a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection