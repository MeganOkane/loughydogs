@extends('layouts.master')

@section('content')

    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:300px;margin-top:43px;">

        <!-- Header -->
        <header class="w3-container" style="padding-top:22px">
            <h5><b> <i class="fa fa-users fa-fw"></i> Manage Users</b></h5>
        </header>

        <table class="table table-striped table-hover text-center">
            <thead>
            <tr>
                <th class="text-center">Dog ID</th>
                <th class="text-center">Owner</th>
                <th class="text-center">Name</th>
                <th class="text-center">Age</th>
                <th class="text-center">Breed</th>
                <th class="text-center">Colour</th>
                <th class="text-center">Gender</th>
                <th class="text-center">Requirements</th>
                <th class="text-center">Image</th>
            </tr>
            </thead>
            <tbody>

            <?php loadDogs() ?>

            </tbody>
        </table>
    </div>

@endsection