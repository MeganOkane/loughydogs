@extends('layouts.master')

@section('content')

    <div class="w3-main" style="margin-left:300px;margin-top:43px;">

        <header class="w3-container" style="padding-top:22px">
            <h5><b>	<i class="fa fa-pencil-square-o fa-fw"></i>Edit My Details</b></h5>
        </header>

        <table class="table table-striped table-hover text-center">
            <thead>
            <tr>
                <th class="text-center">Firstname</th>
                <th class="text-center">Lastname</th>
                <th class="text-center">Email</th>
                <th class="text-center">Home Number</th>
                <th class="text-center">Mobile Number</th>
                <th class="text-center">Address</th>
                <th class="text-center">City</th>
                <th class="text-center">Country</th>
                <th class="text-center">Postcode</th>
                <th class="text-center">Date of Birth</th>
            </tr>
            </thead>
            <tbody>

            <?php displayUserInfo(); ?>

            </tbody>
        </table>


        <body>

        <div class="container">
            <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-default btn-lg" id="myBtn">Edit My Details</button>

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header" style="padding:35px 50px;">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                            <?php displayUserModal();

                            ?>

                        </div>

                    </div>
                </div>
            </div>

            <script>
                $(document).ready(function(){
                    $("#myBtn").click(function(){
                        $("#myModal").modal();
                    });
                });
            </script>


        </div>

@endsection