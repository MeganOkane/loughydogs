<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//----------------//
//    HOMEPAGE   //
//--------------//

Route::get('/', 'HomeController@create')->name('home');

Route::get('/index', 'HomeController@create')->name('home');

Route::post('/home/post', 'HomeController@postContact')->name('homeContact');


//----------------//
//    ABOUT US   //
//--------------//

Route::get('/aboutus', function () {

    return view('pages.aboutus')->with('uri_tail', "about_us");
});

//----------------//
//    GALLERY    //
//--------------//

Route::get('/gallery', function () {

    return view('pages.gallery')->with('uri_tail', "gallery");
});

//--------------//
//   SERVICES  //
//------------//

Route::get('/services', function () {

    return view('pages.services')->with('uri_tail', "services");
});

//--------------------------------//
//   FREQUENTLY ASKED QUESTIONS  //
//------------------------------//

Route::get('/faqs', function () {

    return view('pages.faqs')->with('uri_tail', "faq");
});

//----------------//
//   CONTACT US  //
//--------------//

Route::get('contact', 'ContactUsController@contactUs')->name('getContact');
Route::get('contactus', 'ContactUsController@contactUs')->name('getContact');

Route::post('/contact',  'ContactUsController@postContact');

//--------------------//
//   PRIVACY POLICY  //
//------------------//

Route::get('privacy', function () {

    return view('pages.privacy');
});

Route::get('privacypolicy', function () {

    return view('pages.privacy');
});

Route::get('privacy-policy', function () {

    return view('pages.privacy');
});

//--------------------------//
//   TERMS AND CONDITIONS  //
//------------------------//

Route::get('terms', function () {

    return view('pages.terms');
});

Route::get('terms-and-conditions', function () {

    return view('pages.terms');
});


Route::get('termsandconditions', function () {

    return view('pages.terms');
});

//--------------------------//
//        EXCEPTIONS       //
//------------------------//

Route::get('404', function () {

    return view('pages.errors.404');
});


//---------------------------------------//
//   SESSION ROUTES - DISABLED IN v1.0  //
//-------------------------------------//
//Route::get('/login', function () {
//
//    return view('sessions.login');
//});
//
//Route::post('/login', 'SessionsController@store');
//
//Route::get('/logout', 'SessionsController@destroy');

//--------------------------------------------//
//   REGISTRATION ROUTES - DISABLED IN v1.0  //
//------------------------------------------//

//Route::get('/register', function () {
//
//    return view('registration.create');
//});
//
//Route::post('/register', 'RegistrationController@store');

//---------------------------------------------//
//   LOGGED IN USERS PAGE - DISABLED IN v1.0  //
//-------------------------------------------//

//Route::get('/user', function () {
//
//    return view('user.user');
//});

//-------------------------------------//
//   ADMIN ROUTES - DISABLED IN v1.0  //
//-----------------------------------//

//Route::get('/admin', function () {
//
//    return view('admin.admin');
//});
//
//Route::get('/edituser', function () {
//
//    return view('admin.edituser');
//});
//
//
//Route::get('/manageusers', function () {
//
//    return view('admin.manageusers');
//});
//
//Route::get('/managedogs', function () {
//
//    return view('admin.managedogs');
//});

